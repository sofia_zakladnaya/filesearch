﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace FileSearch
{
    public class SearchCriteria
    {
        [XmlAttribute]
        public string initialDirectory;

        [XmlAttribute]
        public bool allDirectories;

        [XmlAttribute]
        public string fileNameTemplate;

        [XmlAttribute]
        public string textInFile;


        public static string defaultTemplate => "*.*";
        public static string defaultDirectory => Directory.GetCurrentDirectory();
        public static string defaultText => string.Empty;
        public static string criteriaFilePath => Directory.GetCurrentDirectory() + "\\criteria.xml";

        //Конструкторы
        public SearchCriteria()
        {
            initialDirectory = defaultDirectory;
            fileNameTemplate = defaultTemplate;
            textInFile = defaultText;
            allDirectories = true;
        }

        public SearchCriteria(string dir, bool allDir, string templ, string txt)
        {
            if (dir != null)
            {
                initialDirectory = dir;
            }
            else
            {
                initialDirectory = defaultDirectory;
            }
            if (templ != null)
            {
                fileNameTemplate = templ;
            }
            else
            {
                fileNameTemplate = defaultTemplate;
            }
            if (txt != null)
            {
                textInFile = txt;
            }
            else
            {
                textInFile = defaultText;
            }

            allDirectories = allDir;
        }

        //Сохранение критериев в файл
        public void SaveCriteria()
        {

            XmlSerializer serializer = new XmlSerializer(typeof(SearchCriteria));
            FileStream stream = new FileStream(criteriaFilePath, FileMode.Create);

            serializer.Serialize(stream, this);
            stream.Close();
        }

        //Загрузка критериев из файла
        public void ReadCriteria()
        {

            XmlSerializer serializer = new XmlSerializer(typeof(SearchCriteria));
            FileStream stream = new FileStream(criteriaFilePath, FileMode.Open);

            SearchCriteria criteria = (SearchCriteria)serializer.Deserialize(stream);
            stream.Close();

            initialDirectory = criteria.initialDirectory;
            fileNameTemplate = criteria.fileNameTemplate;
            textInFile = criteria.textInFile;
            allDirectories = criteria.allDirectories;
            
        }


    }


}
