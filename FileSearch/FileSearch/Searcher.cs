﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Drawing;

namespace FileSearch
{
    class Searcher
    {
        public SearchCriteria searchCriteria;

        public List<TreeNode> treeNodes;
        public static int fileCount;

        //Конструкторы
        public Searcher()
        {
            searchCriteria = new SearchCriteria();

            List<TreeNode> treeNodes = new List<TreeNode>();
        }

        public Searcher(SearchCriteria criteria)
        {
            searchCriteria = criteria;
            List<TreeNode> treeNodes = new List<TreeNode>();
        }

        public Searcher(string initialDir, bool allDir, string nameTemplate, string textInFile)
        {
            searchCriteria = new SearchCriteria(initialDir, allDir, nameTemplate, textInFile);

            List<TreeNode> treeNodes = new List<TreeNode>();
        }

        //Методы
        public void Search()
        {            
            treeNodes = new List<TreeNode>();
                           
            string[] fullDirectories = Directory.GetDirectories(searchCriteria.initialDirectory);
            string[] fullFiles = Directory.GetFiles(searchCriteria.initialDirectory, searchCriteria.fileNameTemplate, SearchOption.TopDirectoryOnly);
            foreach(string dir in fullDirectories)
            {
                    
                TreeNode node = new TreeNode();
                node.Text = dir.Remove(0, Directory.GetParent(dir).FullName.Length + 1);
               
                //Перебираем подпапки
                if (searchCriteria.allDirectories)
                {
                    Searcher searcher = new Searcher(searchCriteria);
                    searcher.searchCriteria.initialDirectory = dir;
                    searcher.Search();
                    if(searcher.treeNodes.Count>0)
                    {
                        foreach(TreeNode n in searcher.treeNodes)
                        {
                            node.Nodes.Add(n);
                        }
                    }
                }
                if (node.Nodes.Count > 0)
                {
                    treeNodes.Add(node);
                }

            }
            
            foreach (string file in fullFiles)
            {
                try
                {
                    Searcher.fileCount++;
                    if (CheckFile(file))
                    {
                        TreeNode node = new TreeNode(file.Remove(0, Directory.GetParent(file).FullName.Length + 1));
                        node.NodeFont = new Font(TreeView.DefaultFont, FontStyle.Regular);
                        treeNodes.Add(node);
                        
                    }
                }
                catch
                {

                }
            }
            
        }

        public bool CheckFile(string path)
        {
            bool res = false;
            try
            {
                StreamReader str = new StreamReader(path, Encoding.Default);
                while (!str.EndOfStream && !res)
                {
                    string st = str.ReadLine();
                    if (st.Contains(searchCriteria.textInFile))
                    {
                        res = true;
                    }
                }
            }
            catch
            {
               
            }

            return res;
        }
    }
}
