﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;


namespace FileSearch
{
    public partial class Form1 : Form
    {

        private TimeSpan searchTime;
        private DateTime start;
       

        public Form1()
        {
            InitializeComponent();
           
            //выгрузка критериев из файла
            SearchCriteria criteria = new SearchCriteria();
            try
            {
                criteria.ReadCriteria();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
               
            }      
            //Вывод критериев на форму
            tbInitialDir.Text = criteria.initialDirectory;
            tbFileNameTemplate.Text = criteria.fileNameTemplate;
            tbTextInFile.Text = criteria.textInFile;
            chbIncSubDir.Checked = criteria.allDirectories;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SearchCriteria criteria = new SearchCriteria(tbInitialDir.Text, chbIncSubDir.Checked,tbFileNameTemplate.Text,tbTextInFile.Text);
            try
            {
                criteria.SaveCriteria();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            SearchCriteria criteria = new SearchCriteria();
            tbInitialDir.Text = criteria.initialDirectory;
            tbFileNameTemplate.Text = criteria.fileNameTemplate;
            tbTextInFile.Text = criteria.textInFile;
            treeView1.Nodes.Clear();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {

                tbInitialDir.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            CheckCriteriaResult result = checkCriteria();
            switch (result)
            {
                case CheckCriteriaResult.OK:
                    {
                        if (backgroundWorker1.IsBusy != true)
                        {
                            treeView1.Nodes.Clear();
                            Searcher.fileCount = 0;
                            stlFileCounter.Text = "0";
                            stlTime.Text = "00ч:00мин:00сек:00мс";
                            btnCancel.Enabled = true;
                            btnBrowse.Enabled = false;
                            btnReset.Enabled = false;
                            btnSearch.Enabled = false;
                            backgroundWorker1.RunWorkerAsync();

                            start = DateTime.Now;
                            searchTimer.Start();
                            
                        }
                        break;
                    }
                case CheckCriteriaResult.EmptyDir:
                    {
                        MessageBox.Show("Выберите директорию для поиска");
                        break;
                    }
                case CheckCriteriaResult.PathNotExist:
                    {
                        MessageBox.Show("Указанный путь не существует");
                        break;
                    }
                case CheckCriteriaResult.EmptyFilePattern:
                    {
                        tbFileNameTemplate.Text = SearchCriteria.defaultTemplate;

                        if (backgroundWorker1.IsBusy != true)
                        {
                            treeView1.Nodes.Clear();
                            Searcher.fileCount = 0;
                            stlFileCounter.Text = "0";
                            stlTime.Text = "00ч:00мин:00сек:00мс";
                            btnCancel.Enabled = true;
                            btnBrowse.Enabled = false;
                            btnReset.Enabled = false;
                            btnSearch.Enabled = false;
                            backgroundWorker1.RunWorkerAsync();
                            start = DateTime.Now;
                            searchTimer.Start();
                        }
                        break;
                    }
            }
            
        }

        private CheckCriteriaResult checkCriteria()
        {
            if(tbInitialDir.Text == string.Empty)
            {
                return CheckCriteriaResult.EmptyDir;
            }
            if (!Directory.Exists(tbInitialDir.Text))
            {
                return CheckCriteriaResult.PathNotExist;
            }

            if (tbFileNameTemplate.Text == string.Empty)
            {
                return CheckCriteriaResult.EmptyFilePattern;
            }


            return CheckCriteriaResult.OK;
        }

        private void StartSearch()
        {
            SearchCriteria criteria = new SearchCriteria(tbInitialDir.Text, chbIncSubDir.Checked, tbFileNameTemplate.Text, tbTextInFile.Text);
            Searcher searcher = new Searcher(criteria);
            treeView1.Nodes.Clear();

            Thread thread = new Thread(new ThreadStart(searcher.Search));
            thread.IsBackground = true;
            thread.Start();
           
            foreach (TreeNode node in searcher.treeNodes)
            {
                treeView1.Nodes.Add(node);
            }
            treeView1.ExpandAll();
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            Searcher searcher = new Searcher(tbInitialDir.Text, chbIncSubDir.Checked, tbFileNameTemplate.Text, tbTextInFile.Text);
            searcher.treeNodes = new System.Collections.Generic.List<TreeNode>();
           

            string[] fullDirectories = Directory.GetDirectories(tbInitialDir.Text);
            string[] fullFiles = Directory.GetFiles(tbInitialDir.Text, tbFileNameTemplate.Text, SearchOption.TopDirectoryOnly);

            foreach (string dir in fullDirectories)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    TreeNode node = new TreeNode();
                    node.Text = dir.Remove(0, Directory.GetParent(dir).FullName.Length + 1);

                    //Перебираем подпапки
                    if (searcher.searchCriteria.allDirectories)
                    {
                        Searcher searcher1 = new Searcher(searcher.searchCriteria);
                        searcher1.searchCriteria.initialDirectory = dir;
                        searcher1.Search();
                        if (searcher1.treeNodes.Count > 0)
                        {
                            foreach (TreeNode n in searcher1.treeNodes)
                            {
                                node.Nodes.Add(n);
                            }
                        }
                    }
                    if (node.Nodes.Count > 0)
                    {
                        searcher.treeNodes.Add(node);
                        worker.ReportProgress(0, node);
                    }
                }
            }

            foreach (string file in fullFiles)
            {
                try
                {
                    TreeNode node = new TreeNode(file.Remove(0, Directory.GetParent(file).FullName.Length + 1));
                    node.NodeFont = new Font(TreeView.DefaultFont, FontStyle.Regular);
                    Searcher.fileCount++;
                    if (searcher.CheckFile(file))
                    {
                        searcher.treeNodes.Add(node);

                        worker.ReportProgress(0, node);
                    }
                }
                catch
                {

                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            TreeNode node = e.UserState as TreeNode;
            treeView1.Nodes.Add(node);
            stlFileCounter.Text = Convert.ToString(Searcher.fileCount);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            searchTimer.Stop();
            btnCancel.Enabled = false;
            btnBrowse.Enabled = true;
            btnReset.Enabled = true;
            btnSearch.Enabled = true;

            if (searchTime.Milliseconds==0)
            {
                stlTime.Text = "00ч:00мин:00сек:" + (searchTime.TotalMilliseconds) + "мс";
            }
            if(treeView1.Nodes.Count==0)
            {
                treeView1.Nodes.Add("Ничего не найдено");
            }

            if (e.Cancelled == true)
            {
                treeView1.ExpandAll();

            }
            else if (e.Error != null)
            {
                MessageBox.Show("Error: " + e.Error.Message);
            }
            else
            {
               
                treeView1.ExpandAll();
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.WorkerSupportsCancellation == true)
            {
                searchTimer.Stop();
               backgroundWorker1.CancelAsync();
            }
        }

        private void searchTimer_Tick(object sender, EventArgs e)
        {
            searchTime = DateTime.Now - start;

            stlTime.Text = Convert.ToString(searchTime.Hours) + "ч:" + Convert.ToString(searchTime.Minutes) + "мин:" + Convert.ToString(searchTime.Seconds) + "сек:" + Convert.ToString(searchTime.Milliseconds) + "мс";
        }
    }

    enum CheckCriteriaResult
    {
        OK,
        EmptyDir,
        PathNotExist,
        EmptyFilePattern
    }
}
