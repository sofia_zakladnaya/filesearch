﻿namespace FileSearch
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.tbInitialDir = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.chbIncSubDir = new System.Windows.Forms.CheckBox();
            this.tbFileNameTemplate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbTextInFile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.stlTimeLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.stlTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.stlFileCounterLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.stlFileCounter = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.bsSearchResults = new System.Windows.Forms.BindingSource(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.searchTimer = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsSearchResults)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Стартовая директория";
            // 
            // tbInitialDir
            // 
            this.tbInitialDir.Location = new System.Drawing.Point(158, 47);
            this.tbInitialDir.Name = "tbInitialDir";
            this.tbInitialDir.Size = new System.Drawing.Size(516, 20);
            this.tbInitialDir.TabIndex = 1;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(680, 45);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Выбрать";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // chbIncSubDir
            // 
            this.chbIncSubDir.AutoSize = true;
            this.chbIncSubDir.Checked = true;
            this.chbIncSubDir.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbIncSubDir.Location = new System.Drawing.Point(158, 87);
            this.chbIncSubDir.Name = "chbIncSubDir";
            this.chbIncSubDir.Size = new System.Drawing.Size(128, 17);
            this.chbIncSubDir.TabIndex = 3;
            this.chbIncSubDir.Text = "Искать в подпапках";
            this.chbIncSubDir.UseVisualStyleBackColor = true;
            // 
            // tbFileNameTemplate
            // 
            this.tbFileNameTemplate.Location = new System.Drawing.Point(158, 115);
            this.tbFileNameTemplate.Name = "tbFileNameTemplate";
            this.tbFileNameTemplate.Size = new System.Drawing.Size(230, 20);
            this.tbFileNameTemplate.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Шаблон имени файла";
            // 
            // tbTextInFile
            // 
            this.tbTextInFile.Location = new System.Drawing.Point(525, 115);
            this.tbTextInFile.Name = "tbTextInFile";
            this.tbTextInFile.Size = new System.Drawing.Size(230, 20);
            this.tbTextInFile.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(438, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Текст в файле";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stlTimeLabel,
            this.stlTime,
            this.stlFileCounterLabel,
            this.stlFileCounter});
            this.statusStrip1.Location = new System.Drawing.Point(0, 992);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // stlTimeLabel
            // 
            this.stlTimeLabel.Name = "stlTimeLabel";
            this.stlTimeLabel.Size = new System.Drawing.Size(45, 17);
            this.stlTimeLabel.Text = "Время:";
            // 
            // stlTime
            // 
            this.stlTime.Name = "stlTime";
            this.stlTime.Size = new System.Drawing.Size(0, 17);
            // 
            // stlFileCounterLabel
            // 
            this.stlFileCounterLabel.Name = "stlFileCounterLabel";
            this.stlFileCounterLabel.Size = new System.Drawing.Size(126, 17);
            this.stlFileCounterLabel.Text = "Обработано файлов: ";
            // 
            // stlFileCounter
            // 
            this.stlFileCounter.Name = "stlFileCounter";
            this.stlFileCounter.Size = new System.Drawing.Size(0, 17);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(158, 160);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 11;
            this.btnSearch.Text = "Поиск";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Enabled = false;
            this.btnCancel.Location = new System.Drawing.Point(239, 160);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(320, 160);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(199, 23);
            this.btnReset.TabIndex = 13;
            this.btnReset.Text = "Сбросить критерии поиска";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(155, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Результаты поиска";
            // 
            // treeView1
            // 
            this.treeView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.treeView1.Location = new System.Drawing.Point(158, 235);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(582, 745);
            this.treeView1.TabIndex = 16;
            // 
            // searchTimer
            // 
            this.searchTimer.Interval = 1;
            this.searchTimer.Tick += new System.EventHandler(this.searchTimer_Tick);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 1014);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tbTextInFile);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbFileNameTemplate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chbIncSubDir);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.tbInitialDir);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Поиск файлов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsSearchResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbInitialDir;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.CheckBox chbIncSubDir;
        private System.Windows.Forms.TextBox tbFileNameTemplate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbTextInFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripStatusLabel stlTimeLabel;
        private System.Windows.Forms.BindingSource bsSearchResults;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Timer searchTimer;
        private System.Windows.Forms.ToolStripStatusLabel stlTime;
        private System.Windows.Forms.ToolStripStatusLabel stlFileCounterLabel;
        private System.Windows.Forms.ToolStripStatusLabel stlFileCounter;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

